import React from 'react';
import {View, Text, StyleSheet, Image} from 'react-native';

import {Cast} from '../interfaces/creditsInterface';

interface ActorInterface {
  actor: Cast;
}

export const ActorItem = ({actor}: ActorInterface) => {
  const uri = `https://image.tmdb.org/t/p/w500${actor.profile_path}`;

  return (
    <View style={styles.container}>
      {actor.profile_path && <Image source={{uri}} style={styles.actorImage} />}
      <View style={styles.actorInfo}>
        <Text style={styles.actorName}>{actor.name}</Text>
        <Text style={styles.character}>{actor.name}</Text>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 3,
    },
    shadowOpacity: 0.38,
    shadowRadius: 3,
    backgroundColor: 'white',
    borderRadius: 10,
    paddingRight: 15,
    height: 50,
    marginRight: 20,
  },
  actorInfo: {
    marginLeft: 10,
  },
  actorName: {
    marginTop: 4,
    fontSize: 18,
    fontWeight: 'bold',
  },
  character: {
    fontSize: 16,
    opacity: 0.8,
  },
  actorImage: {
    width: 50,
    aspectRatio: 1,
    borderRadius: 10,
  },
});
