import React, {useCallback} from 'react';
import {View, Text, StyleSheet, FlatList} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';

import {Cast} from '../interfaces/creditsInterface';
import {FullMovie} from '../interfaces/movieInterface';
import {ActorItem} from './ActorItem';

interface MovieData {
  fullMovie: FullMovie;
  actors: Cast[];
}

export const MovieDetails = ({fullMovie, actors}: MovieData) => {
  const keyExtractor = useCallback((actor: Cast) => `${actor.cast_id}`, []);

  return (
    <>
      <View style={styles.detailsContainer}>
        <View style={styles.row}>
          <Icon name="star-outline" color="grey" size={16} />
          <Text>{fullMovie.vote_average}</Text>
          <Text> - {fullMovie.genres.map(g => g.name).join(', ')}</Text>
        </View>

        <Text style={styles.title}>History</Text>
        <Text style={styles.text}>{fullMovie.overview}</Text>

        <Text style={styles.title}>Budget</Text>
        <Text style={styles.text}>
          {new Intl.NumberFormat('de-DE', {
            style: 'currency',
            currency: 'USD',
          }).format(fullMovie.budget)}
        </Text>
      </View>

      <View style={styles.castContainer}>
        <Text style={[styles.title, styles.margin]}>Actors</Text>
        <FlatList
          horizontal
          data={actors}
          renderItem={({item}) => <ActorItem actor={item} />}
          keyExtractor={keyExtractor}
          showsHorizontalScrollIndicator={false}
          style={styles.actorList}
        />
      </View>
    </>
  );
};

const styles = StyleSheet.create({
  detailsContainer: {
    marginHorizontal: 20,
  },
  row: {
    flexDirection: 'row',
  },
  title: {
    marginTop: 10,
    fontSize: 23,
    fontWeight: 'bold',
  },
  text: {
    fontSize: 16,
  },
  margin: {
    marginHorizontal: 20,
  },
  castContainer: {},
  actorList: {
    marginTop: 10,
    height: 70,
    paddingLeft: 20,
    marginBottom: 30,
  },
});
