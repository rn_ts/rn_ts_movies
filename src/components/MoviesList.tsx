import React from 'react';
import {View, Text, StyleSheet, FlatList} from 'react-native';
import {Movie} from '../interfaces/movieInterface';
import {MoviePoster} from './MoviePoster';

interface Props {
  title?: string;
  data: Movie[];
}

const Styles = StyleSheet.create({
  listsContainer: {
    height: 320,
    justifyContent: 'center',
  },
  titleSection: {
    fontSize: 30,
    fontWeight: 'bold',
    color: 'black',
    marginHorizontal: 10,
  },
  listStyle: {
    paddingTop: 10,
  },
});

export const MoviesList = ({title, data}: Props) => {
  return (
    <View style={Styles.listsContainer}>
      <Text style={Styles.titleSection}>{title}</Text>
      <FlatList
        style={Styles.listStyle}
        data={data}
        keyExtractor={item => item.id.toString()}
        renderItem={({item}) => (
          <View>
            <MoviePoster movie={item} width={140} height={200} />
          </View>
        )}
        showsHorizontalScrollIndicator={false}
        horizontal
      />
    </View>
  );
};
