import {useNavigation} from '@react-navigation/native';
import {NativeStackNavigationProp} from '@react-navigation/native-stack';
import React from 'react';
import {View, Image, StyleSheet, TouchableOpacity} from 'react-native';
import {Movie} from '../interfaces/movieInterface';
import {RootStackParams} from '../navigation';

interface Props {
  movie: Movie;
  height?: number;
  width?: number;
}

export const MoviePoster = ({movie, height = 420, width = 300}: Props) => {
  const {poster_path} = movie;

  const uri = `https://image.tmdb.org/t/p/w500${poster_path}`;

  const navigation =
    useNavigation<NativeStackNavigationProp<RootStackParams>>();

  return (
    <TouchableOpacity
      onPress={() => navigation.navigate('DetailScreen', movie)}
      style={[Styles.container, {height, width}]}
      activeOpacity={0.8}>
      <View style={Styles.imageShadow}>
        <Image
          source={{
            uri,
          }}
          style={Styles.image}
          resizeMode="cover"
        />
      </View>
    </TouchableOpacity>
  );
};

const Styles = StyleSheet.create({
  container: {
    paddingHorizontal: 5,
  },
  image: {
    flex: 1,
    borderRadius: 18,
  },
  imageShadow: {
    flex: 1,
    borderRadius: 18,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 12,
    },
    shadowOpacity: 0.58,
    shadowRadius: 16.0,

    elevation: 24,
  },
});
