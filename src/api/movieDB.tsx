import axios from 'axios';

const movieDB = axios.create({
  baseURL: 'https://api.themoviedb.org/3/movie',
  params: {
    api_key: 'd0516f86cd1430b9ecc39428fe7babca',
    language: 'en-US',
  },
});

export default movieDB;
