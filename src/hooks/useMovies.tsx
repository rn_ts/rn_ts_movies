import {useEffect, useState} from 'react';
import movieDB from '../api/movieDB';
import {Movie, MovieDBResponse} from '../interfaces/movieInterface';

interface MoviesState {
  nowMovies: Movie[];
  popularMovies: Movie[];
  topRated: Movie[];
  upcoming: Movie[];
}

export const useMovies = () => {
  const [movies, setMovies] = useState<MoviesState>({
    nowMovies: [],
    popularMovies: [],
    topRated: [],
    upcoming: [],
  });
  const [isLoading, setIsLoading] = useState<Boolean>(true);

  const getMovies = async () => {
    const nowMoviesPromise = movieDB.get<MovieDBResponse>('/now_playing');
    const popularMoviesPromise = movieDB.get<MovieDBResponse>('/popular');
    const topRatedPromise = movieDB.get<MovieDBResponse>('/top_rated');
    const uncomingPromise = movieDB.get<MovieDBResponse>('/upcoming');

    const response = await Promise.all([
      nowMoviesPromise,
      popularMoviesPromise,
      topRatedPromise,
      uncomingPromise,
    ]);

    setMovies({
      nowMovies: response[0].data.results,
      popularMovies: response[1].data.results,
      topRated: response[2].data.results,
      upcoming: response[3].data.results,
    });

    setIsLoading(false);
  };

  useEffect(() => {
    getMovies();
  }, []);

  return {...movies, isLoading};
};
