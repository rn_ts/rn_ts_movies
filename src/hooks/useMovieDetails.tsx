import {useState, useEffect, useCallback} from 'react';
import movieDB from '../api/movieDB';
import {MovieCredits} from '../interfaces/creditsInterface';
import {FullMovie} from '../interfaces/movieInterface';

interface MovieDetails {
  isLoading: boolean;
  credits?: MovieCredits;
  fullMovie?: FullMovie;
}
export const useMovieDetails = (movieId: number) => {
  const [state, setFullMovie] = useState<MovieDetails>({
    isLoading: true,
    credits: undefined,
    fullMovie: undefined,
  });

  const getMovieDetails = useCallback(async () => {
    const movieDetailPromise = movieDB.get<FullMovie>(`/${movieId}`);
    const creditsPromise = movieDB.get<MovieCredits>(`/${movieId}/credits`);

    const [{data: dataDetails}, {data: dataCredits}] = await Promise.all([
      movieDetailPromise,
      creditsPromise,
    ]);

    setFullMovie({
      isLoading: false,
      fullMovie: dataDetails,
      credits: dataCredits,
    });
  }, [movieId]);

  useEffect(() => {
    getMovieDetails();
  }, [getMovieDetails]);

  return {
    ...state,
  };
};
