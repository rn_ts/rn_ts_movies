import React, {useContext, useEffect} from 'react';
import {
  View,
  ActivityIndicator,
  Dimensions,
  StyleSheet,
  ScrollView,
} from 'react-native';
import Carousel from 'react-native-reanimated-carousel';
// import {NativeStackScreenProps} from '@react-navigation/native-stack';
import {useSafeAreaInsets} from 'react-native-safe-area-context';

import {useMovies} from '../hooks/useMovies';
import {MoviePoster} from '../components/MoviePoster';
import {MoviesList} from '../components/MoviesList';
import {BackgroundGradient} from '../components/BackgroundGradient';
import {getColors} from '../helpers/getColors';
import {GradientContext} from '../context/GradientContext';

// interface Props extends NativeStackScreenProps<any, any> {}

const {width} = Dimensions.get('window');

const styles = StyleSheet.create({
  loadingContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  container: {
    paddginTop: 20,
    justifyContent: 'center',
  },
  carouselStyle: {
    width,
    justifyContent: 'center',
    marginBottom: 20,
  },
});

export const HomeScreen = () => {
  const {top} = useSafeAreaInsets();
  const {nowMovies, popularMovies, topRated, upcoming, isLoading} = useMovies();
  const {setMainColors} = useContext(GradientContext);

  const getPostedColors = async (index: number) => {
    const {poster_path} = nowMovies[index];
    const uri = `https://image.tmdb.org/t/p/w500${poster_path}`;
    const [primary = 'red', secondary = 'blue'] = await getColors(uri);
    setMainColors({primary, secondary});
  };

  useEffect(() => {
    if (nowMovies.length > 0) {
      getPostedColors(0);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [nowMovies]);

  if (isLoading) {
    return (
      <View style={styles.loadingContainer}>
        <ActivityIndicator color="red" size="large" />
      </View>
    );
  }

  return (
    <BackgroundGradient>
      <ScrollView>
        <View style={[styles.container, {top}]}>
          <Carousel
            loop
            autoPlay={false}
            style={styles.carouselStyle}
            width={300}
            height={450}
            data={nowMovies}
            mode="parallax"
            modeConfig={{
              parallaxScrollingScale: 0.9,
              parallaxScrollingOffset: 30,
            }}
            renderItem={({item}) => <MoviePoster movie={item} />}
            onSnapToItem={getPostedColors}
          />
        </View>
        <MoviesList title="Popular movies" data={popularMovies} />
        <MoviesList title="Top rated" data={topRated} />
        <MoviesList title="Uncoming" data={upcoming} />
      </ScrollView>
    </BackgroundGradient>
  );
};
