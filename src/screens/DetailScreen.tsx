import React from 'react';
import {
  View,
  Image,
  StyleSheet,
  Dimensions,
  ScrollView,
  Text,
  ActivityIndicator,
  TouchableOpacity,
} from 'react-native';
import {NativeStackScreenProps} from '@react-navigation/native-stack';
import Icon from 'react-native-vector-icons/Ionicons';
import {Movie} from '../interfaces/movieInterface';
import {RootStackParams} from '../navigation';

import {useMovieDetails} from '../hooks/useMovieDetails';
import {MovieDetails} from '../components/MovieDetails';

const {height} = Dimensions.get('screen');

interface Props
  extends NativeStackScreenProps<RootStackParams, 'DetailScreen'> {}

export const DetailScreen = ({route, navigation}: Props) => {
  const movie = route.params as Movie;
  const {isLoading, fullMovie, credits} = useMovieDetails(movie.id);

  const uri = `https://image.tmdb.org/t/p/w500${movie.poster_path}`;
  return (
    <ScrollView
      bounces={false}
      contentContainerStyle={styles.contentContainerStyle}>
      <View style={styles.imageContainer}>
        <Image source={{uri}} style={styles.posterImage} />
      </View>
      <View style={styles.marginContainer}>
        <Text style={styles.subtitle}>{movie.title}</Text>
        <Text style={styles.title}>{movie.original_title}</Text>
      </View>
      {isLoading ? (
        <ActivityIndicator size={'large'} color={'gray'} />
      ) : (
        <MovieDetails fullMovie={fullMovie!} actors={credits?.cast!} />
      )}
      <TouchableOpacity
        onPress={() => navigation.goBack()}
        style={styles.backContainer}
        activeOpacity={0.8}>
        <Icon name="arrow-back-outline" color="white" size={25} />
      </TouchableOpacity>
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  contentContainerStyle: {},
  imageContainer: {
    width: '100%',
    height: height * 0.7,
    borderBottomRightRadius: 25,
    borderBottomLeftRadius: 25,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 12,
    },
    shadowOpacity: 0.58,
    shadowRadius: 16.0,
    elevation: 24,
  },
  posterImage: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    borderBottomRightRadius: 25,
    borderBottomLeftRadius: 25,
  },
  marginContainer: {
    marginTop: 20,
    marginHorizontal: 20,
  },
  title: {
    fontSize: 20,
    fontWeight: 'bold',
  },
  subtitle: {
    fontSize: 16,
    opacity: 0.8,
  },
  backContainer: {
    height: 40,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'grey',
    aspectRatio: 1,
    borderRadius: 100,
    position: 'absolute',
    top: 40,
    left: 20,
    zIndex: 999,
    elevation: 9,
  },
});
