import 'react-native-gesture-handler';
import React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {Navigation} from './src/navigation';
import {GestureHandlerRootView} from 'react-native-gesture-handler';
import {GradientProvider} from './src/context/GradientContext';

const GESTURE_HANDLER_ROOT_VIEW_STYLES = {flex: 1};

const AppState = ({children}: any) => {
  return <GradientProvider>{children}</GradientProvider>;
};

const App = () => {
  return (
    <GestureHandlerRootView style={GESTURE_HANDLER_ROOT_VIEW_STYLES}>
      <NavigationContainer>
        <AppState>
          <Navigation />
        </AppState>
      </NavigationContainer>
    </GestureHandlerRootView>
  );
};

export default App;
